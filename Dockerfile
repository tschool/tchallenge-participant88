FROM node:14
COPY source/. /source
WORKDIR /source
RUN npm install -g python3
RUN npm install 
CMD ["npm", "run", "start", "--", "--host", "0.0.0.0", "--disableHostCheck", "true"]

# Expose ports.
EXPOSE 80:80
EXPOSE 4300:4300
EXPOSE 27017:27017
EXPOSE 443:443
EXPOSE 4200:4200
EXPOSE 4567:4567



